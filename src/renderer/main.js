import Vue from 'vue'
import axios from 'axios'

import App from './App'
import router from './router'
import store from './store'
import lodash from 'lodash'
import moment from 'moment'

import Vuetify from 'vuetify'

import 'vuetify/dist/vuetify.css'
import '@fortawesome/fontawesome-free/css/all.css'

Vue.use(Vuetify, {
  iconfont: 'fa4'
})

const ullagetable = require("./assets/ullagetable.json");

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.prototype.ullagetable = ullagetable
Vue.prototype.$_ = lodash
Vue.prototype.moment = moment

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  moment,
  template: '<App/>'
}).$mount('#app')
